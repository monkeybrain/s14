console.log('External JS Script');


let num = 10;
console.log(num);

let name = 'Marites';
console.log(name);


let num2 = 5;
console.log(num2);

num2 = 5 + 3;
console.log(num2);

let brand = 'Toyota', model = 'Vios', type = 'Sedan';
console.log(brand);
console.log(model);
console.log(type);

console.log(brand, model, type);

let country = 'Philippines';
let province = 'Manila';	

console.log(country, province);


//Mini Activity
let firstName = "Marvin";
let lastName = "Dacian";



let fullName = firstName + lastName;

console.log(fullName);

let word1 = 'is';
let word2 = 'student';
let word3 = 'of';
let word4 = 'Zuitt';
let word5 = 'Coding';
let word6 = 'Bootcamp';
let space = ' ';


let sentence = (fullName + space + word1 + space + word2 + space + word3 + space + word4 + space + word5 + space + word6);

console.log(sentence);

let sentenceLiteral = `${fullName} ${word1} ${word2} ${word3} ${word4} ${word5} ${word6}`;

console.log(sentenceLiteral);

//number data type

let numString1 = '7';
let numString2 = '5';
let numA = 7;
let numB = 5;
let num3 = 5.5;
let num4 = .5;

console.log(numString1 + numString2);
console.log(numA + numB);
console.log(numA + num3);
console.log(num3 + num4);

//forced coercion - when one data type is FORCED to change to complete an operation

//string + num = concatenation
console.log(numString1 + numA);//'77' resulted in concatenation
console.log(num3 + numString2);//5.56

//parseInt() - a function; this can change the type of numeric string to a proper number

console.log(num4 + parseInt(numString1));//7.5 numString1 was parsed into a proper number instead of a string
console.log(num4 + numString1);//0.57

let sum = numA + parseInt(numString1);
console.log(sum);

//Mathematical Operations (-,*,/,%)
//subtraction
	console.log(num3 - numA);//-1.5
	console.log(numString1 - numB);//2 results in proper mathematical operation because the string was forced to become a number. strings can be subtracted
	console.log(numString1 - numString2);// In subtraction, numeric string will not concatenate and instead will be forcibly changed into a proper mathematical operation without the need to use parseInt().

//multiplication
	console.log(numA * numB); //35
	console.log(numString1 * numA);//49
	console.log(numString1 * numString2)//35


	let product = numA * numB;
	let product2 = numString1 * numA;
	let product3 = numString1 * numString2;

//division
	console.log(product / numB);//7

//Arrays - group of data
let array = ['luffy', 'zoro', 'zanji', 'chopper', 'ussop', 'nami', 'robin'];
console.log(array);

let array1 = ['One Punch Man', true, 500, 'Saitama'];
console.log(array1);


//Objects
	//objects are another special kind of data type used to mimic real world objects
	//used to create complex data that contain pieces of information that are relevant to each other
	//an object is created with an object literal {}
	//each field is called a property
	//each field is separated by comma

let hero = {
	heroName: 'One Punch Man',
	isActive: true,
	salary: 500,
	realName: "Saitama",
	height: 200
};

console.log(hero);	


let EHeads = ['Ely', 'Raymond', 'Marcus', 'Buddy'];

console.log(EHeads);

let Ely = {
	firstName: 'Ely',
	lastName: 'Buendia',
	isDeveloper: 'False',
	hasPortfolio: 'True',
	age: 56
};

console.log(Ely);


//function
function greet() {
	console.log('Hello my name is Dr.GreenThumb');
}

//invoke a function
greet(); //this will display the function invoked

// parameters and arguments
//A parameter acts a name variable/container that exists Only inside of the function. This is used as a way to store information to act as a stand-in or the container the value passed in as argument

function printName(name){
	console.log(`My name is ${name}`)
}
printName('marvin');
//When function is invoked and data is passed, we call the data as argument
//In this invocation, 'marvin' is an "argument" passed into our printName function and is represented by the "name" parameter within our function

function displayNum(number){
	console.log(`The number is ${number}`);
};
displayNum(5000);

//Multiple Parameters and Arguments
	//A function can not only receive singlee argument but also multiple arguments as long as it matches the number of parameter

function displayFullName(firstName, lastName, age) {
	console.log(`${firstName}, ${lastName}, ${age}`);
	}

displayFullName('Juan', 'Thugs', 29);

//return keyword
function createName(firstName, lastName){
	return `${firstName} ${lastName}`
	console.log("will no longer run")
}

let fullName1 = createName('Donald', 'Trump');
console.log(fullName1);


//includes javascript intro activity - commit message

function addition (value1, value2){
	console.log(value1 + value2);
}

addition(1,2);


function subtraction (value1, value2){
	console.log(value1 - value2);
}

subtraction(1,2);


function multiplication (value1, value2){
	return (value1 * value2)
}

let answer = multiplication(3,3);
console.log(answer);

